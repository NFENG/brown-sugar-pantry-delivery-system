import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * This class encapsulates the route optimisation logic and JSON Parsing
 */

public class PathEvaluator {
    int minRouteValue = 0;
    String path = "";
    String bestPath = "";

    public PathEvaluator() {
    }

    /**
     * This method creates the possible routes of all the given destinations
     * @param addresses
     * @return path
     */
    public String createPath(ArrayList<Address> addresses) {
        int addressLength = addresses.size();
        for (int i = addressLength; i > 1; i--) {
            path += i;
        }
        return path;
    }

    /**
     * This method parses the JSON file from the Google Distance Matrix API and returns the best possible route
     * in terms of time taken.
     * @param jsonArray
     * @param routeCombination
     * @return bestPath String
     */
    public String evaluate(JsonArray jsonArray, ArrayList<String> routeCombination) {

        for (int i = 0; i < routeCombination.size(); i++) {

            routeCombination.set(i,routeCombination.get(i).concat("1"));
            int previousNode = 1;
            int currentValue = 0;

            for (int j = 0; j < routeCombination.get(i).length(); j++) {

                JsonObject jsonObject1 = jsonArray.get(previousNode - 1).getAsJsonObject();
                JsonArray jsonArray1 = jsonObject1.getAsJsonArray("elements");

                char charValue = routeCombination.get(i).charAt(j);
                int intValue = Character.getNumericValue(charValue);

                JsonObject jsonObject2 = jsonArray1.get(intValue-1).getAsJsonObject();
                JsonObject jsonObject3 = jsonObject2.get("duration").getAsJsonObject();

                currentValue += jsonObject3.get("value").getAsInt();
                previousNode = intValue;
            }
            if (minRouteValue == 0 || currentValue < minRouteValue) {
                minRouteValue = currentValue;
                bestPath = routeCombination.get(i);
            }
            else {
            }
        }

        return bestPath;
    }

}
