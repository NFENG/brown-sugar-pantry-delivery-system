/**
 * Object class for the addresses of each destination.
 */
public class Address {
    private String houseNumber;
    private String streetName;
    private String suburb;
    private String postcode;
    private String state;
    private String country;

    /**
     * constructor for Address
     * @param houseNumber
     * @param streetName
     * @param suburb
     * @param postcode
     * @param state
     * @param country
     */
    public Address(String houseNumber, String streetName, String suburb, String postcode, String state, String country) {
        this.houseNumber = houseNumber;
        this.streetName = streetName;
        this.suburb = suburb;
        this.postcode = postcode;
        this.state = state;
        this.country = country;
    }
    public Address() {
    }

    /**
     * getters and setters
     */

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Formats the Address object to be written into a CSV file
     * @return String
     */
    public String toString() {
        return houseNumber + "," + streetName + "," + suburb + "," + postcode + "," + state + "," +
                country;
    }
}
