import com.google.appengine.repackaged.com.google.api.client.json.Json;
import com.google.gson.*;
import com.google.maps.model.Distance;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.util.ArrayList;


/**
 * This class is in relation to the distances between destinations for the route optimiser.
 */
public class DistanceMatrix {

    private static final String apiKey = "AIzaSyCZT9bn7C9rIRcQUMquEkOXvouzFGFNuxw";
    private String matrixURL = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";

    public DistanceMatrix() {
    }

    /**
     * Formats Google Maps distanceMatrix URL to be used in the method getData()
     * @param addresses
     */
    public void encodeURL(ArrayList addresses) {
        // Origin addresses
        for (int i =0; i < addresses.size();i++) {
            matrixURL += addresses.get(i).toString();
            if (i != addresses.size() - 1) {
                matrixURL += "|";
            } else {
            }
        }
        // Destination addresses
        matrixURL += "&destinations=";
        for (int i =0; i < addresses.size();i++) {
            matrixURL += addresses.get(i).toString();
            if (i != addresses.size() - 1) {
                matrixURL += "|";
            }
            else {
            }
        }
        matrixURL += "&key=";
        matrixURL += apiKey;
    }

    /**
     * This method resets the URL to default
     */
    public void defaultURL() {
        matrixURL = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=";
    }



    /**
     *  This method build url required for the API calls using the distance matrix
     * @throws IOException
     */
    public JsonObject getData() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(matrixURL).method("GET", null).build()
                ;
        Response response = client.newCall(request).execute();
        String responseString = response.body().string();
        JsonObject jsonTree = (JsonObject) JsonParser.parseString(responseString);
        return jsonTree;

    }
}
