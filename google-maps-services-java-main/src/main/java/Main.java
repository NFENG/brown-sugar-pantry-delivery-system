import com.google.gson.*;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws IOException {


        String paths;
        String bestPath;

        ArrayList<String> routeCombination;
        ArrayList<Address> addresses = new ArrayList<>();
        DistanceMatrix distanceMatrix = new DistanceMatrix();

        Permutation factor = new Permutation();
        CSVReader reader = new CSVReader();
        PathEvaluator pathEvaluator = new PathEvaluator();

        //reads the CSV file of addresses
        reader.readFile(addresses);
        // encodes the addresses into a URL, for API usage
        distanceMatrix.encodeURL(addresses);
        // distance matrix returned as JSON file
        JsonObject jsonTree = distanceMatrix.getData();
        JsonArray jsonArray = jsonTree.getAsJsonArray("rows");

        // creates all possible routes for the given destinations
        paths = pathEvaluator.createPath(addresses);
        routeCombination = factor.permutation(paths);
        // finds the best route for the given destinations
        bestPath = pathEvaluator.evaluate(jsonArray, routeCombination);
        // writes in the csv file "address.csv"
        reader.writeFile(bestPath,addresses);
    }
}