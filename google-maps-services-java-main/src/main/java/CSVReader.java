import java.io.*;
import java.util.ArrayList;

/**
 * This class is in relation to the necessary methods for reading and writing CSV files.
 */
public class CSVReader {
    int count = 0;
    final File file = new File("C:\\Users\\natha\\BSPDeliverySystem" +
            "\\brown-sugar-pantry-delivery-system\\res\\address1.txt");

    /**
     * Constructor for the file reader
     */
    public CSVReader() {
    }

    /**
     * Mutates the addresses arrayList by reading the CSV file and creating Address objects and adding them
     * into the addresses arrayList.
     *
     * @param addresses
     * @throws IOException
     */
    protected void readFile(ArrayList<Address> addresses) throws IOException {
        try (BufferedReader br =
                     new BufferedReader(new java.io.FileReader(file))) {
            String text;
            while ((text = br.readLine()) != null) {
                String[] columns = text.split(",");

                if (count != 0) {
                    addresses.add(new Address(columns[0], columns[1], columns[2], columns[3], columns[4], columns[5]));
                } else {
                    count = 1;
                }

            }
        }

    }

    /**
     * This method writes into the file "address.csv" of the optimal route from top to bottom and the addresses of each
     * destination
     * @param bestPath
     * @param addresses
     */
    protected void writeFile(String bestPath, ArrayList<Address> addresses) {
        File Output = new File( "C:\\Users\\natha\\brown-sugar-pantry-delivery-system\\google-maps-services-" +
                "java-main", "address.csv");
        try (FileWriter writer = new FileWriter(Output, false)) {
            for (int i = 0; i < bestPath.length(); i++) {
                int index = Character.getNumericValue(bestPath.charAt(i));
                writer.write(addresses.get(index - 1).toString());
                writer.write('\n');
            }
            writer.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
